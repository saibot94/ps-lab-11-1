def main():
    for number in xrange(1, 101):
        div3 = number % 3 == 0
        div5 = number % 5 == 0
        if div3 and div5:
            print 'FizzBuzz'
        elif div3:
            print 'Fizz'
        elif div5:
            print 'Buzz'
        else:
            print number

if __name__ == '__main__':
    main()
