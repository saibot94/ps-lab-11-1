var request = new XMLHttpRequest();

var method = "GET";
var url = "https://www.reddit.com/r/programming.json";

function showData(children) {
    for(var i = 0; i < children.length; i++) {
        var article = children[i].data;
        // TODO
        // document.
        document
            .writeln("<p>" + 
                article.ups + " - " + article.title + "</p>");
    }
}

request.open(method, url, true);
function handleRequest() { 
    if(request.readyState == XMLHttpRequest.DONE) {
        var response = JSON.parse(request.responseText);
        console.log("After downloading... ");
        showData(response.data.children);
    }
}
request.onreadystatechange = handleRequest;


request.send();
console.log("After calling send()");
